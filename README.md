# Exo2Condensed Font
Exo2 Condensed Font for Magisk.
This module includes *condensed* type of typeface *only for nonitalic*. Italic typeface are from original Exo2.

## Instructions ##
* __Install Module__ via Magisk Manager/Recovery
* __Reboot__ Device

## Thanks
Natanael Gama [<a href="http://www.ndiscovered.com/"> homepage </a>]

## Screenshot ##
<img src="https://ffont.ru/upls/previews/8550_3c0dfa9f67d9266c0f55e266e050e9d2.jpg" alt="Screenshot">
